import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Sale } from './sale.entity';
import { Repository } from 'typeorm';
import { PaymentsService } from 'src/payments/payments.service';
import { ItemsService } from 'src/items/items.service';

@Injectable()
export class SalesService {

    constructor(
        @InjectRepository(Sale) private readonly saleRepository: Repository<Sale>,
        private paymentService: PaymentsService,
        private itemsService: ItemsService
    ) {}

    async create( data: any): Promise<Sale>{

        let items = data.items;
        
        let itemsIDs = [];
        for (const item of data.items) {
            itemsIDs.push(item.id);
        }

        data.items = itemsIDs;

        const sale = await this.saleRepository.save( data );

        for (const item of items) {
           let itemDB = await this.itemsService.findOne( item.id );

           itemDB.stock -= item.count;

           this.itemsService.update( itemDB.id, itemDB );
        }

        data.payment.sale = sale.id;

        await this.paymentService.create( data.payment );

        return sale;
        
    }

    async findAll(): Promise<Sale[]> {
        return await this.saleRepository.find( { relations: ['client'] } );
    }

    async lastId(): Promise<number> {
        let sales = await this.saleRepository.find({ order: { 'id': 'DESC' }});

        let lastId = 0;

        if( sales.length ) {

            lastId = sales[0].id;
        }

        return lastId + 1;
    }
}


