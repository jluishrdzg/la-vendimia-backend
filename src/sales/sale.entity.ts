import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinTable, ManyToMany } from 'typeorm';
import { Client } from 'src/clients/client.entity';
import { Item } from 'src/items/item.entity';

@Entity()
export class Sale {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Client, client => client.id)
  client: Client;

  @ManyToMany(type => Item, items => items.id, {
    cascade: true
  })
  @JoinTable()
  items: Item[];

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  downpayment: string;
  
  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  downpaymentBonus: number;
  
  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  total: number;

  @Column({ name: 'date', type: 'datetime', default: () => "CURRENT_TIMESTAMP" }) 
  date: Date;




  
}