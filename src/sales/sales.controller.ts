import { Controller, Post, Body, Get, Query } from '@nestjs/common';
import { SalesService } from './sales.service';
import { Sale } from './sale.entity';

@Controller('sales')
export class SalesController {

    constructor(private readonly salesService: SalesService) {}

    @Post()
    create(@Body() confuration: Sale) {
      return this.salesService.create( confuration );
    }

    @Get()
    findAll(@Query() query: any) {
      return this.salesService.findAll();
    }

    @Get('lastid')
    lastId(@Query() query: any) {
      return this.salesService.lastId();
    }

}
