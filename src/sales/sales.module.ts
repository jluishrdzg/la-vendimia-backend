import { Module } from '@nestjs/common';
import { SalesController } from './sales.controller';
import { SalesService } from './sales.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Sale } from './sale.entity';
import { PaymentsService } from 'src/payments/payments.service';
import { ItemsService } from 'src/items/items.service';
import { PaymentsModule } from 'src/payments/payments.module';
import { ItemsModule } from 'src/items/items.module';

@Module({
  imports: [TypeOrmModule.forFeature([Sale]), PaymentsModule, ItemsModule],
  controllers: [SalesController],
  providers: [SalesService, PaymentsService, ItemsService],
  exports: [TypeOrmModule]
})
export class SalesModule {}
