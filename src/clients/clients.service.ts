import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from './client.entity';
import { Repository, UpdateResult, DeleteResult, Raw, Like } from 'typeorm';

@Injectable()
export class ClientsService {

    constructor(
        @InjectRepository(Client) private readonly clientRepository: Repository<Client>
    ) {}

    async create( client: Client): Promise<Client>{
        return await  this.clientRepository.save( client )
    }

    async findAll( query ): Promise<Client[]> {
        let options = {}

        if ( query.query ) {
            options = {
                where : [{
                    firstname : Like('%'+ query.query +'%'),
                }, {
                    lastname : Like('%'+query.query+'%'),
                }]             
            }
        }

        return await this.clientRepository.find(options);
    }

    async findOne( id: number ): Promise<Client> {
        return await this.clientRepository.findOne( id );
    }

    async update( id: number, client: Client ): Promise<UpdateResult> {
        return await this.clientRepository.update( id, client );
    }

    async delete( id: number): Promise<DeleteResult> {
        return await this.clientRepository.delete( id );
    }

    async lastId(): Promise<number> {
        let clients = await this.clientRepository.find({ order: { 'id': 'DESC' }});

        let lastId = 0;

        if( clients.length ) {

            lastId = clients[0].id;
        }

        return lastId + 1;
    }

}
