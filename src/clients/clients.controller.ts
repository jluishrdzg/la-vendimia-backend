import { Controller, Get, Post, Body, Query, Param, Delete, Put } from '@nestjs/common';
import { ClientsService } from './clients.service';
import { Client } from './client.entity';

@Controller('clients')
export class ClientsController {

  constructor(private readonly clientService: ClientsService) {}

    @Post()
    create(@Body() client: Client) {
      return this.clientService.create( client );
    }
  
    @Get()
    findAll(@Query() query: any) {
      return this.clientService.findAll( query );
    }

    @Get('lastid')
    lastId(@Query() query: any) {
      return this.clientService.lastId();
    }
  
    @Get(':id')
    findOne(@Param('id') id: number) {
      return this.clientService.findOne( id );
    }
  
    @Put(':id')
    update(@Param('id') id: number, @Body() client: any) {
      return this.clientService.update( id, client );
    }
  
    @Delete(':id')
    remove(@Param('id') id: number) {
      return this.clientService.delete( id );
    }


}
