import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: false })
  firstname: string;

  @Column({ length: 100, nullable: false })
  lastname: string;

  @Column({ length: 13, unique: true, nullable: false })
  rfc: string;

}