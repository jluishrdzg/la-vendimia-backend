import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Item {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500, nullable: false })
  description: string;

  @Column({ length: 100, nullable: false })
  model: string;
  
  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  price: number;
  
  @Column('int', { nullable: false })
  stock: number;
  
}