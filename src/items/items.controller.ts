import { Controller, Post, Get, Query, Body, Put, Param, Delete } from '@nestjs/common';
import { ItemsService } from './items.service';
import { Item } from './item.entity';

@Controller('items')
export class ItemsController {

    constructor(private readonly itemsService: ItemsService) {}

    @Post()
    create(@Body() item: Item) {
      return this.itemsService.create( item );
    }
  
    @Get()
    findAll(@Query() query: any) {
      return this.itemsService.findAll( query );
    }

    @Get('lastid')
    lastId(@Query() query: any) {
      return this.itemsService.lastId();
    }

    @Get('stock/:id')
    stock(@Param('id') id: number, @Query() query: any) {
      return this.itemsService.stock( id, query );
    }
  
    @Get(':id')
    findOne(@Param('id') id: number) {
      return this.itemsService.findOne( id );
    }
  
    @Put(':id')
    update(@Param('id') id: number, @Body() item: any) {
      return this.itemsService.update( id, item );
    }
  
    @Delete(':id')
    remove(@Param('id') id: number) {
      return this.itemsService.delete( id );
    }

}
