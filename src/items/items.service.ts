import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult, UpdateResult, Raw } from 'typeorm';
import { Item } from './item.entity';

@Injectable()
export class ItemsService {

    constructor(
        @InjectRepository(Item) private readonly itemtRepository: Repository<Item>
    ) {}

    async create( item: Item): Promise<Item>{
        return await  this.itemtRepository.save( item )

        
    }

    async findAll( query ): Promise<Item[]> {
        let options = {}

        if ( query.query ) {
            options = {
                where: {
                    description: Raw(description => `${description} like '%${query.query}%'`),
                },
            }
        }

        return await this.itemtRepository.find(options);
    }

    async findOne( id: number ): Promise<Item> {
        return await this.itemtRepository.findOne( id );
    }

    async update( id: number, item: Item ): Promise<UpdateResult> {
        return await this.itemtRepository.update( id, item );
    }

    async delete( id: number): Promise<DeleteResult> {
        return await this.itemtRepository.delete( id );
    }

    async lastId(): Promise<number> {
        let items = await this.itemtRepository.find({ order: { 'id': 'DESC' }});

        let lastId = 0;

        if( items.length ) {

            lastId = items[0].id;
        }

        return lastId + 1;
    }

    async stock( id: number, query: any): Promise<Boolean> {

        let count = 1;
    
        if ( query.count ) {
            count = query.count;
        }

        let item = await this.itemtRepository.findOne( id );

        return item.stock >= count;
    }
    
}
