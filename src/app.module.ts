import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule } from './clients/clients.module';
import { Client } from './clients/client.entity';
import { Item } from './items/item.entity';
import { ItemsModule } from './items/items.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { Configuration } from './configuration/configuration.entity';
import { SalesModule } from './sales/sales.module';
import { Sale } from './sales/sale.entity';
import { Payment } from './payments/payment.entity';
import { PaymentsModule } from './payments/payments.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '',
        database: 'lavendimia',
        entities: [ Client, Item, Configuration, Sale, Payment ],
        synchronize: true,
    }),
    ClientsModule,
    ItemsModule,
    ConfigurationModule,
    SalesModule,
    PaymentsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
