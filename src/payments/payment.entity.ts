import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from 'typeorm';
import { Sale } from 'src/sales/sale.entity';

@Entity()
export class Payment {

  @PrimaryGeneratedColumn()
  id: number;

  @Column('int')
  count: number;

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  toPayment: number;

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  totalToPayment: number;

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  saving: number;

  @OneToOne(type => Sale)
  @JoinColumn()
  sale: Sale;


  
}