import { Module } from '@nestjs/common';
import { Payment } from './payment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentsService } from './payments.service';

@Module({
    imports: [TypeOrmModule.forFeature([Payment])],
    controllers: [],
    providers: [PaymentsService],
    exports: [TypeOrmModule]
})
export class PaymentsModule {}
