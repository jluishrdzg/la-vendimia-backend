import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Payment } from './payment.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PaymentsService {

    constructor(
        @InjectRepository(Payment) private readonly paymentRepository: Repository<Payment>
    ) {}

    async create( payment: Payment): Promise<Payment>{
        return await  this.paymentRepository.save( payment )   
    }
}
