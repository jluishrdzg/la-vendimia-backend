import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { Configuration } from './configuration.entity';

@Injectable()
export class ConfigurationService {

    constructor(
        @InjectRepository(Configuration) private readonly configurationRepository: Repository<Configuration>
    ) {}

    async create( configuration: Configuration): Promise<Configuration>{
        return await  this.configurationRepository.save( configuration )
    }

    async findOne( id: number ): Promise<Configuration> {
        return await this.configurationRepository.findOne( id );
    }

    async update( id: number, configuration: Configuration ): Promise<UpdateResult> {
        return await this.configurationRepository.update( id, configuration );
    }
    
}
