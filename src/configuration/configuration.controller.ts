import { Controller, Post, Get, Param, Body, Put } from '@nestjs/common';
import { ConfigurationService } from './configuration.service';
import { Configuration } from './configuration.entity';

@Controller('configuration')
export class ConfigurationController {

    constructor(private readonly configurationService: ConfigurationService) {}

    @Post()
    create(@Body() confuration: Configuration) {
      return this.configurationService.create( confuration );
    }
  
    @Get(':id')
    findOne(@Param('id') id: number) {
      return this.configurationService.findOne( id );
    }
  
    @Put(':id')
    update(@Param('id') id: number, @Body() confuration: any) {
      return this.configurationService.update( id, confuration );
    }

}
