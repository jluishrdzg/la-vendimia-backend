import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Configuration {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  rate: number;

  @Column('decimal', { precision: 15, scale: 2, nullable: false })
  downpayment: number;

  @Column('int', { nullable: false})
  deadline: number;

}